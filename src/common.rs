
use gdnative::prelude::*;



#[repr(u8)]
#[derive(Copy, Clone)]
pub enum MidiKind {
    NoteOff = 0,
    NoteOn = 1,
    NoteKeyPressure = 2,
    SetParameter = 3,
    SetProgram = 4,
    ChangePressure = 5,
    PitchBend = 6,
    MetaEvent = 7
   }

impl From<u8> for MidiKind {
    fn from(orig: u8) -> Self {
        match orig {
            0 => { return Self::NoteOff}
            1 => { return Self::NoteOn}
            2 => { return Self::NoteKeyPressure}
            3 => { return Self::SetParameter}
            4 => { return Self::SetProgram}
            5 => { return Self::ChangePressure}
            6 => { return Self::PitchBend}
            _ => { return Self::MetaEvent}
        };
    }
}