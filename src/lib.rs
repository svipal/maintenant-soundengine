#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(deprecated)]
#![allow(dead_code)]
#![allow(unsafe_code)]
#![macro_use]


#[macro_use]
extern crate lazy_static;
extern crate libc;
extern crate num;

use gdnative::*;
use gdnative::prelude::*;
use std::mem;
use std::ffi::CString;
use std::ffi::CStr;
use std::ptr::*;
use std::boxed::Box;
use std::collections::HashMap;
use crossbeam::atomic::AtomicCell;
// use std::alloc::{alloc, dealloc, Layout};

use std::thread;
use std::fmt;
use std::time::Duration;

include!("../bindings/bindings.rs");

use dashmap::DashMap; 

mod errors;
mod common;
use common::*;
use errors::*;

lazy_static! {
  static ref SFPATH : String = "./assets/soundfonts/".to_string();
}

macro_rules! mkStr {
  ($s:expr) => {CString::from_raw($s as *mut _).into_string().unwrap()}
}


macro_rules! mkStr2 {
  ($s:expr) => {CStr::from_ptr($s as *mut _).to_str().unwrap()}
}
macro_rules! mkGStr {
  ($s:expr) => {GodotString::from_str($s)}
}

macro_rules! mkGStrR {
  ($s:expr) => {GodotString::from_str(mkStr!($s))}
}

macro_rules! sanitize {
  ($type:ty,$ident:ident) => {
    $ident = Box::into_raw(Box::<$type>::new(*$ident));
  }
}

macro_rules! wealloc {
  ($type:ty) => {
    libc::malloc( mem::size_of::<$type>()as libc::size_t) as *mut $type;
  }
}

macro_rules! sigString{
  ($($arg:tt)*) => {{
    let r = [Variant::from_str(format!($($arg)*))]; 
    r
  }}
  
}

static beatPower : AtomicCell<f32>  = AtomicCell::new(0.);
// static beatCount : AtomicCell<i32>  = AtomicCell::new(0);
static beatLock  : AtomicCell<bool> = AtomicCell::new(false);
static position  : AtomicCell<i32>  = AtomicCell::new(0);
static bar       : AtomicCell<i32>  = AtomicCell::new(0);


static beatProps : AtomicCell<FMOD_STUDIO_TIMELINE_BEAT_PROPERTIES> 
        = AtomicCell::new(
          FMOD_STUDIO_TIMELINE_BEAT_PROPERTIES {
            bar: 0, 
            beat: 0, 
            position: 0, 
            tempo: 0., 
            timesignatureupper: 0, 
            timesignaturelower: 0, 
        });


macro_rules! makeCString {
  ($string:expr) => {
    CString::new($string.clone()).unwrap();
  }
}

macro_rules! makeRawCString {
  ($string:expr) => {
    CString::new($string.clone()).unwrap().into_raw();
  }
}

macro_rules! beat {
  () => {
    (beatProps.load()).beat as i64
  }
}
macro_rules! bar {
  () => {
    (beatProps.load()).bar as i64
  }
}
macro_rules! position {
  () => {
    (beatProps.load()).position as i64
  }
}
macro_rules! tempo {
  () => {
    (beatProps.load()).tempo as f64
  }
}
macro_rules! tupper {
  () => {
    (beatProps.load()).timesignatureupper as i64
  }
}
macro_rules! tlower {
  () => {
    (beatProps.load()).timesignaturelower as i64
  }
}

unsafe extern "C" fn callbackTest(t: FMOD_STUDIO_EVENT_CALLBACK_TYPE,
  event: *mut FMOD_STUDIO_EVENTINSTANCE,
  parameters: *mut std::os::raw::c_void) -> FMOD_RESULT {
    match t {
      FMOD_STUDIO_EVENT_CALLBACK_TIMELINE_BEAT => {
        beatPower.store(100.);
        let props = parameters as *mut FMOD_STUDIO_TIMELINE_BEAT_PROPERTIES;
        beatProps.store(*props);
        return FMOD_RESULT_FMOD_OK;
      }
      _ => {return 0;}
    }
}

#[derive(NativeClass)]
#[inherit(Node)]
#[register_with(Self::register_signals)]
pub struct SoundEngine{
  fmodSys:Option<*mut FMOD_STUDIO_SYSTEM>,
  fmodCoreSys:Option<*mut FMOD_SYSTEM>,
  events : HashMap<String,*mut *mut FMOD_STUDIO_EVENTDESCRIPTION>,
  eventInstance : Option<*mut FMOD_STUDIO_EVENTINSTANCE>,
  banks :  HashMap<String,*mut *mut FMOD_STUDIO_BANK>,

  fsSettings: Option<*mut fluid_settings_t>,
  fSynth:     Option<*mut fluid_synth_t>,
  fsDriver:    Option<*mut fluid_audio_driver_t>,

}


fn newSoundEngine() -> SoundEngine {
  return SoundEngine {
    //FMOD
    fmodSys: None,
    fmodCoreSys: None,
    events: HashMap::new(),
    eventInstance: None,
    banks: HashMap::new(),
    //fluidsynth
    fsSettings: None,
    fSynth: None,
    fsDriver: None,
  }; 
}

#[methods]
impl SoundEngine {
  fn new(_owner: &Node) -> Self {
    godot_print!("ye goode soundengine");
    return newSoundEngine();
  }

  #[export]
  unsafe fn _ready(&mut self, owner: TRef<Node>) {
    let inputHandler = &mut owner.get_node("/root/InputHandler").unwrap();
    let midiHandler = &mut owner.get_node("/root/MidiHandler").unwrap();
    let inputHandler = inputHandler.assume_safe();
    let midiHandler = midiHandler.assume_safe();

    midiHandler
        .connect("midiMessage", owner, "_on_MIDI_Message_received", VariantArray::new_shared(), 0)
        .unwrap();
    // inputHandler 
    //     .connect("midiMessage", owner, "_on_MIDI_Message_received", VariantArray::new_shared(), 0)
    //     .unwrap();

  }

  #[export]
  unsafe fn _on_MIDI_Message_received(&mut self, 
      owner: &Node, 
      timestamp: Variant, 
      header: Variant, 
      message: Variant){
    self.playMIDI(owner, header,message);
  }

  #[export]
  unsafe fn playMIDI(
   &mut self, 
   owner: &Node, 
   header: Variant, 
   message: Variant){
    let header_ : TypedArray<u8>  = header.to_byte_array();
    let message_ : TypedArray<u8>  = message.to_byte_array();
    self.fSynth.map(|synth| {
      match header_.get(0).into() {
        MidiKind::NoteOn => {
          let x = fluid_synth_noteon(
            synth,
            header_.get(1).into(),  //channel
            message_.get(1).into(), //note
            message_.get(2).into() //velo
          );
          match x {
            FLUID_OK => {
              let bp =beatPower.load() / 100.;
              let NotePrecision = bp;
              owner.emit_signal("Note Precision", &sigString!("{}",NotePrecision));  
            }
            _ => {
              godot_print!("no");
            }
          }
        }
        MidiKind::NoteOff => {
          let x = fluid_synth_noteoff(
            synth,
            header_.get(1).into(),  //channel
            message_.get(1).into(), //note
          );
          match x {
            FLUID_OK => {}
            _ => {
              godot_print!("couldn't send note off msg");
            }
          }
        }
        MidiKind::SetProgram => {
          let newprog : u8= message_.get(1).into();
          let x = fluid_synth_program_change(
            synth,
            header_.get(1).into(),  //channel
            message_.get(1).into(), //value
          );
          match x {
            FLUID_OK => {
              owner.emit_signal("Prog Change", &[Variant::from_i64(newprog as i64)]);  
            }
            _ => {
              godot_print!("couldn't send pc message");
            }
          }
        }
        _ => {}
      }
    });
  }

  #[export]
  unsafe fn progChange(&mut self, owner: &Node, ch: u8, prog: i32) -> Option<i32>{
    self.fSynth.map(|synth| {
      let x = fluid_synth_program_change(
        synth,
        ch as i32,
        prog.into(), //value
      );
      match x {
        FLUID_OK => {
          owner.emit_signal("Prog Change", &[Variant::from_i64(ch as i64),Variant::from_i64(prog as i64)]);
          return prog;  
        }
        _ => {
          godot_print!("couldn't send pc message");
          return -1;
        }
      }
    })
  }

  #[export]
  unsafe fn initSystem(&mut self, owner: &Node, userSettings: Dictionary<Shared>) {
    // fluidsynth stuff
    let mut settings = new_fluid_settings();
    let mut driver = wealloc!(fluid_audio_driver_t);
    godot_print!("{} detected",std::env::consts::OS);

    macro_rules! i32setting {
      ($name:expr,$default:expr) => {
        userSettings.get($name).try_to_i64().unwrap_or_else(||{
          godot_print!("couldn't load {}, switching to default : {}",$name,$default);
          $default}) as i32
      }
    }

    macro_rules! f64setting {
      ($name:expr,$default:expr) => {
        userSettings.get($name).try_to_f64().unwrap_or_else(||{
          godot_print!("couldn't load {}, switching to default : {}",$name,$default);
          $default})
      }
    }

    macro_rules! strsetting {
      ($name:expr,$default:expr) => {
        userSettings.get($name).try_to_string().unwrap_or_else(||{
          godot_print!("couldn't load {}, switching to default : {}",$name,$default);
          $default.to_string()})
      }
    }
    let driver = strsetting!("fs_audiodriver","portaudio");
  
   
    if driver == "jack" {
        fluid_settings_setint(settings, 
          makeRawCString!("audio.jack.autoconnect"), 
          1);

        fluid_settings_setstr(settings, 
          makeRawCString!("audio.jack.id"), 
          makeRawCString!("Maintenant"));

      }
    let result = fluid_settings_setstr(settings,  
      makeRawCString!("audio.driver"),  
      makeRawCString!(driver));
    let result2 = fluid_settings_setint(settings, 
      makeRawCString!("synth.polyphony"), 
      i32setting!("fs_polyphony",1024));
    let result3 = fluid_settings_setint(settings, 
      makeRawCString!("synth.cpu-cores"), 
      i32setting!("fs_cores",3));
    let result4 = fluid_settings_setint(settings, 
      makeRawCString!("audio.period-size"), 
      i32setting!("fs_periodSize",256));
    let result5 = fluid_settings_setnum(settings, 
      makeRawCString!("synth.gain"), 
      f64setting!("fs_gain",2.0));
    let r6 = fluid_settings_setnum(settings, 
      makeRawCString!("synth.sample-rate"), 
      f64setting!("fs_sampleRate",48000.));
    let mut synth= new_fluid_synth(settings);
    let mut synthPtr: *mut *mut fluid_synth_t = &mut synth;
    sanitize!(*mut fluid_synth_t, synthPtr);
    let mut driver = new_fluid_audio_driver(settings, synth);
    self.fsDriver = Some(driver);
    // lmao we don't talk about this ? lole
    // more seriously fluidsynth prints error messages to stderr, we don't need
    // manual error handling
    match result+result2+result3+result4+result5 {
      FLUID_OK => {
        godot_print!("fluidsynth init ok :)");
        self.fsSettings = Some(settings);
        self.fSynth = Some(synth);
      }
      _  => {
        godot_print!("fluidsynth init not ok:(");
        let error =  fluid_synth_error(synth) ;
        println!("{}",mkStr!(error));
        
      }
    }
     
    
    //FMOD stuff
    let mut system : *mut FMOD_STUDIO_SYSTEM = wealloc!(FMOD_STUDIO_SYSTEM);
    let systemPtr : *mut *mut FMOD_STUDIO_SYSTEM = &mut system;
    godot_print!("{}", FMOD_VERSION);
    let result  = FMOD_Studio_System_Create(systemPtr, 131332);
    godot_print!("tried to create FMOD System...");
    match result {
      FMOD_RESULT_FMOD_OK => {
        godot_print!("system created!");
        self.fmodSys = Some(system);  
      }
      err     => {godot_print!("creation error : {}",getFMODErrString(err));}
    }
    let result = FMOD_Studio_System_Initialize(
      system,
      i32setting!("FMOD_channels",32), // number of fmod channel
      FMOD_STUDIO_INIT_LIVEUPDATE,
      FMOD_INIT_NORMAL,
      null_mut());
    match result {
      FMOD_RESULT_FMOD_OK => {
        owner.emit_signal("FMOD System Init", &[]);
        let mut core_system : *mut FMOD_SYSTEM = wealloc!(FMOD_SYSTEM);
        let result = FMOD_Studio_System_GetCoreSystem(
          system,
          &mut core_system,
        );
        match result {
          FMOD_RESULT_FMOD_OK => {owner.emit_signal("Step Notif",&sigString!("Core system successfully extracted..")); }
          err => {owner.emit_signal("Step Notif",&sigString!("Core system couldn't be extracted : {}", getFMODErrString(err)));}
        }
      }
      err     => {godot_print!("init error : {}",getFMODErrString(err));}
    }
  }
  #[export]
  unsafe fn loadFont(&mut self, owner: &Node, instrument: String) -> TypedArray<GodotString> {
    let mut presetNames = Vec::new();
    match self.fSynth {
      Some(synth) => {
        let load_result = fluid_synth_sfload(synth, makeRawCString!(SFPATH.clone()+&instrument+".sf2"), 1);
        match load_result {
          FLUID_FAILED => {
            owner.emit_signal("Step Notif", &sigString!("couldn't load instrument {} ", instrument));
          }
          id =>  {
            let font = fluid_synth_get_sfont_by_id(
              synth,
              id 
            );
            owner.emit_signal("Step Notif",&sigString!("instrument {}, id:{} loaded ! ", instrument, id));
            fluid_sfont_iteration_start(font);
            godot_print!("looking through font preset...");
            loop  {
              godot_print!("next preset..");
              let mut preset = fluid_sfont_iteration_next(font);
              let mut presetPtr : *mut *mut fluid_preset_t = &mut preset;
              sanitize!(*mut fluid_preset_t,presetPtr);
              match preset.as_ref() {
                //no more presets in the soundfont
                None => { godot_print!("no more presets");
                       break;}
                _ => {
                  let name =  fluid_preset_get_name(preset);
                  godot_print!("-"); 	
                  let actualName = mkStr2!(name);
                  godot_print!("{}",actualName); 	
                  presetNames.push(actualName);
                }
              }
            }
          }
        }
      }
      _ => {
        owner.emit_signal("Step Notif", &sigString!("couldn't load instrument {} ", instrument));
      }
    }
    return TypedArray::from_vec(presetNames.iter().map(|str| {
      mkGStr!(str)
    }).collect() );

  }



  #[export]
  unsafe fn loadBank (&mut self, owner: &Node, path: String) {
    let system = self.fmodSys.unwrap();
    let mut bank: *mut FMOD_STUDIO_BANK =  wealloc!(FMOD_STUDIO_BANK);
    let mut bankPtr : *mut *mut FMOD_STUDIO_BANK = &mut bank;
    
    let pathCStr = CString::new(path.clone()).unwrap();
    let result = FMOD_Studio_System_LoadBankFile(
        system,
        pathCStr.into_raw(),
        FMOD_STUDIO_LOAD_BANK_NORMAL,
        bankPtr
        );

    match result {
      FMOD_RESULT_FMOD_OK    => {
        if FMOD_Studio_Bank_IsValid(bank) != 0 {
          godot_print!("bank ok");
        } else {
          godot_print!("bank not ok");
        }
        self.banks.insert(path.clone(),bankPtr);
        owner.emit_signal("Step Notif", &[Variant::from_str(format!("bank at {} loaded", path))]);
        let sampleLoads = FMOD_Studio_Bank_LoadSampleData(bank);
        let mut state = wealloc!(FMOD_STUDIO_LOADING_STATE);
        match sampleLoads {
          FMOD_RESULT_FMOD_OK => {owner.emit_signal("Step Notif", &[Variant::from_str("samples loaded")]);}
          err => {owner.emit_signal("Step Notif", &[Variant::from_str(format!("couldn't load samples : {}", getFMODErrString(err)))]);}
        }
      }
      err => {owner.emit_signal("Step Notif", &[Variant::from_str(format!("could not load bank ,  {}",getFMODErrString(err)))]);}
    }
  }

  #[export]
  unsafe fn loadEvent (&mut self, owner: &Node, path: String) {
    let system = self.fmodSys.unwrap();
    let pathCStr = CString::new(path.clone()).unwrap();
    let mut eventDesc : *mut FMOD_STUDIO_EVENTDESCRIPTION =  wealloc!(FMOD_STUDIO_EVENTDESCRIPTION);
    let mut eventDescPtr  : *mut *mut FMOD_STUDIO_EVENTDESCRIPTION= &mut eventDesc;
    let result = FMOD_Studio_System_GetEvent(
      system,
      pathCStr.into_raw(),
      eventDescPtr
    );
    match result {
      FMOD_RESULT_FMOD_OK   => {
        //We sanitize the pointers AFTER initializing it, for some reason?...
        sanitize!(*mut FMOD_STUDIO_EVENTDESCRIPTION, eventDescPtr);
        
        self.events.insert(path.clone(),eventDescPtr);
        owner.emit_signal("Step Notif", &[Variant::from_str(format!("properly loaded {}", path ))]);
      }
      err => {owner.emit_signal("Step Notif", &[Variant::from_str(format!("could not get event description from system,  {}",getFMODErrString(err)))]);}
    }
  }
  
  #[export]
  unsafe fn createEventInstance (&mut self, owner: &Node, path: String) {
    let mut eventInstance : *mut FMOD_STUDIO_EVENTINSTANCE = wealloc!(FMOD_STUDIO_EVENTINSTANCE);
    let mut eventInstPtr  : *mut *mut FMOD_STUDIO_EVENTINSTANCE= &mut eventInstance;
    
    let eventDesc = self.events.get_mut(&path);
    if eventDesc.is_none() {
      owner.emit_signal("Step Notif", &[Variant::from_str(format!("no such event..."))]);
      return;
    }

    if self.eventInstance.is_some() {
      owner.emit_signal("Step Notif", &[Variant::from_str(format!("unloading previous instance..."))]);
      FMOD_Studio_EventInstance_Release(self.eventInstance.unwrap());
    }

    let result = FMOD_Studio_EventDescription_CreateInstance(
      eventDesc.unwrap().read(),
      eventInstPtr
    );
    match result {
      FMOD_RESULT_FMOD_OK => {
        // //We sanitize the pointer AFTER initializing it, for some reason?...again ??
        sanitize!(*mut FMOD_STUDIO_EVENTINSTANCE, eventInstPtr);
        
        self.eventInstance = Some(eventInstance);
        owner.emit_signal("Step Notif", &[Variant::from_str(format!("event {} instanciated", path ))]);
      }
      err => {owner.emit_signal("Step Notif", &[Variant::from_str(format!("could not create event instance,  {}",getFMODErrString(err)))]);}
    }
  }

  // we don't actually need this because we update in our own _process
  #[export]
  unsafe fn update (&mut self, owner: &Node) {
    let system = self.fmodSys.unwrap();
    let result = FMOD_Studio_System_Update(system);
    match result {
      FMOD_RESULT_FMOD_OK   => {}
      err => {owner.emit_signal("Update Failed", &[Variant::from_str(format!("could not update system ,  {}",getFMODErrString(err)))]);}
    }
  }

  #[export]
  unsafe fn playCurrentInstance (&mut self, owner: &Node) {
      if self.eventInstance.is_none() {
        //Warning
      owner.emit_signal("Step Notif", &[Variant::from_str(format!("no instance to start..."))]);
      return;
    }
    let result = FMOD_Studio_EventInstance_Start(self.eventInstance.unwrap());
    match result {
      
      FMOD_RESULT_FMOD_OK   => {
        FMOD_Studio_EventInstance_SetVolume(
          self.eventInstance.unwrap(),
          3.0
        );
        owner.emit_signal("Step Notif", &[Variant::from_str("Playback successful")]);
      
      
      let result = FMOD_Studio_EventInstance_SetCallback(
        self.eventInstance.unwrap(),
        Some(callbackTest),
        FMOD_STUDIO_EVENT_CALLBACK_ALL);
      }
      err => {owner.emit_signal("Step Notif", &[Variant::from_str(format!("Could not start instance:  ,  {}",getFMODErrString(err)))]);}
    }
  }
  #[export]
  unsafe fn stopCurrentInstance (&mut self, owner: &Node, mode: u8) {
    if self.eventInstance.is_none() {
      //Warning
      owner.emit_signal("Step Notif", &[Variant::from_str(format!("no instance to stop..."))]);
      return;
    }
    let result = FMOD_Studio_EventInstance_Stop(self.eventInstance.unwrap(),
                                                mode as FMOD_STUDIO_STOP_MODE);
    match result {
      FMOD_RESULT_FMOD_OK   => {owner.emit_signal("Step Notif", &[Variant::from_str("current instance playback stopped")]);}
      err => {owner.emit_signal("Step Notif", &[Variant::from_str(format!("Could not stop instance:  ,  {}",getFMODErrString(err)))]);}
      
    }
  }

  #[export]
  unsafe fn setLocalParameter(&mut self, owner: &Node, parameter: String, value :f32, ignoreseekspeed: bool ) {
    let param = CString::new(parameter.clone()).unwrap();
    if self.eventInstance.is_none() {
      owner.emit_signal("Step Notif", &[Variant::from_str(format!("no instance to set parameters to..."))]);
      return;
    }
    let result = FMOD_Studio_EventInstance_SetParameterByName(
      self.eventInstance.unwrap(),
      param.into_raw(),
      value,
      ignoreseekspeed as i32
    );
    match result {
      FMOD_RESULT_FMOD_OK   => {},
      err => {owner.emit_signal("Step Notif", &[Variant::from_str(format!("Could not set parameter:  ,  {}",getFMODErrString(err)))]);}
    }
  }

  fn register_signals(builder: &ClassBuilder<Self>) {
    builder.add_signal(Signal {
      name: "Prog Change",
      args: &[SignalArgument {  
        name: "channel",
        default: Variant::from_i64(0),
        export_info: ExportInfo::new(VariantType::I64),
        usage: PropertyUsage::DEFAULT,
        },
        SignalArgument {  
          name: "prog",
          default: Variant::from_i64(0),
          export_info: ExportInfo::new(VariantType::I64),
          usage: PropertyUsage::DEFAULT,
        }]
    });
    builder.add_signal(Signal {
      name: "FS System Init",
      args: &[]
    });
    builder.add_signal(Signal {
      name: "FMOD System Init",
      args: &[]
    });
    builder.add_signal(Signal {
      name: "Note Precision",
      args: &[ SignalArgument {  
        name: "precision",
        default: Variant::from_f64(1.),
        export_info: ExportInfo::new(VariantType::F64),
        usage: PropertyUsage::DEFAULT,
      }]
    });
    builder.add_signal(Signal {
      name: "Beat",
      args: &[SignalArgument {  
        name: "count",
        default: Variant::from_i64(0),
        export_info: ExportInfo::new(VariantType::I64),
        usage: PropertyUsage::DEFAULT,
        },
        SignalArgument {  
          name: "position",
          default: Variant::from_i64(0),
          export_info: ExportInfo::new(VariantType::I64),
          usage: PropertyUsage::DEFAULT,
        },
        SignalArgument {  
          name: "bar",
          default: Variant::from_i64(0),
          export_info: ExportInfo::new(VariantType::I64),
          usage: PropertyUsage::DEFAULT,
        },
        SignalArgument {  
          name: "tupper",
          default: Variant::from_i64(4),
          export_info: ExportInfo::new(VariantType::I64),
          usage: PropertyUsage::DEFAULT,
        },
        SignalArgument {  
          name: "tdown",
          default: Variant::from_i64(4),
          export_info: ExportInfo::new(VariantType::I64),
          usage: PropertyUsage::DEFAULT,
        },
        SignalArgument {  
          name: "tempo",
          default: Variant::from_f64(130.),
          export_info: ExportInfo::new(VariantType::F64),
          usage: PropertyUsage::DEFAULT,
        }]
    });
    builder.add_signal(Signal {
      name: "Update Failed",
      args: &[SignalArgument {  
                name: "result",
                default: Variant::from_str("OK"),
                export_info: ExportInfo::new(VariantType::GodotString),
                usage: PropertyUsage::DEFAULT,
              }]
    });

    builder.add_signal(Signal {
      name: "Step Notif",
      args: &[SignalArgument {  
                  name: "result",
                  default: Variant::from_str("OK"),
                  export_info: ExportInfo::new(VariantType::GodotString),
                  usage: PropertyUsage::DEFAULT,
                }]
    });
  }



  #[export]
  fn getBeatPower(&mut self, _owner:&Node) -> f32 {
    return beatPower.load();
  }

  
  #[export]
  fn getBeat(&mut self, _owner:&Node) -> i64 {
    return beat!();
  }
  
  #[export]
  unsafe fn setFSGain(&mut self, _owner:&Node, gain: f64) {
    self.fsSettings.map(|settings| {
      let result = fluid_settings_setnum(
        settings, 
        makeRawCString!("synth.gain"), 
        gain);
    });
    
  }

  #[export]
  unsafe fn _process(&mut self, owner: &Node,_delta:f32){
    self.fmodSys.map(|sys|{
      FMOD_Studio_System_Update(sys);
    });
    let bp = beatPower.load();
    beatPower.store(num::clamp::<f32>(bp - _delta*100.,0.,100.));
    if beatLock.load() {
      owner.emit_signal("Beat", &[
        Variant::from_i64(beat!()),
        Variant::from_i64(position!()),
        Variant::from_i64(bar!()),
        Variant::from_i64(tupper!()),
        Variant::from_i64(tlower!()),
        Variant::from_f64(tempo!()),
      ]);
      thread::spawn(move || beatLock.store(false));
      
    }
  }
  #[export]
  unsafe fn clean(&mut self, owner: &Node){
    self.eventInstance.map (|eI| {
      FMOD_Studio_EventInstance_Release(eI);
    });
    self.fmodSys.map (|sys| {
      FMOD_Studio_System_Release(sys);
    });
    owner.emit_signal("Step Notif", &[Variant::from_str("FMOD system cleaned")]);
    // fluidsynth cleaning
    self.fsSettings.map(|settings| {
      delete_fluid_settings(settings);
    });
    self.fSynth.map(|synth| {
      delete_fluid_synth(synth);
    });
    self.fsDriver.map(|driver| {
      delete_fluid_audio_driver(driver);
    });
  }

}

fn init(handle: InitHandle) {
  handle.add_class::<SoundEngine>();
}

godot_gdnative_init!();
godot_nativescript_init!(init);
godot_gdnative_terminate!();

