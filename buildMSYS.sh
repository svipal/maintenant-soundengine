C_INCLUDE_PATH="./FMOD" gcc\
    -Wall\
    -fPIC\
    -O3\
    -c\
    -I./\godot_headers\
    -I.\
    ./$1.c\
    -lfmod\
    -lfmod_studio\
    -o $1.o;
LD_LIBRARY_PATH=D:/\Sound/\Tools/\Jack/\lib gcc \
    -shared\
    $1.o\
    -LD:/\Sound/\Tools/\Jack/\lib\
    -o\
    lib$1-mingw.dll\
    "D:/\Sound/\Tools/\FMOD SoundSystem/\FMOD Studio API Windows/\api/\studio/\lib/\x64/\fmodstudio_vc.lib"\
    "D:/\Sound/\Tools/\FMOD SoundSystem/\FMOD Studio API Windows/\api/\fsbank/\lib/\x64/\fsbank_vc.lib"\
    "D:/\Sound/\Tools/\FMOD SoundSystem/\FMOD Studio API Windows/\api/\core/\lib/\x64/\fmod_vc.lib";
