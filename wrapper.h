
#include "FMOD/fmod.h"
#include "FMOD/fmod_common.h"
#include "FMOD/fmod_errors.h"
#include "FMOD/fmod_output.h"
#include "FMOD/fmod_studio_common.h"
#include "FMOD/fmod_studio.h"
#include "FMOD/fmod_codec.h"
#include "FMOD/fmod_dsp_effects.h"
#include "FMOD/fmod_dsp.h"
#include "FMOD/fsbank_errors.h"
#include "FMOD/fsbank.h"

#include "fluidsynth/fluidsynth.h"