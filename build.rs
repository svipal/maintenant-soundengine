#![feature(link_args)]
#![link_args="-Wl,-rpath ."]
extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {    
println!("cargo:rustc-link-lib=fmod");
println!("cargo:rustc-link-lib=fmodstudio");
println!("cargo:rustc-link-lib=fsbank");
println!("cargo:rustc-link-lib=fluidsynth");
println!("cargo:rustc-link-search=\"./FMODdlls/\"");
println!("cargo:rustc-link-search=\"./fluidsynthdlls/\"");
// Tell cargo to invalidate the built crate whenever the wrapper changes
println!("cargo:rerun-if-changed=wrapper.h");

// The bindgen::Builder is the main entry point
// to bindgen, and lets you build up options for
// the resulting bindings.
let bindings = bindgen::Builder::default()
    // The input header we would like to generate
    // bindings for.
    .header("wrapper.h")
    // Tell cargo to invalidate the built crate whenever any of the
    // included header files changed.
    .parse_callbacks(Box::new(bindgen::CargoCallbacks))
    // Finish the builder and generate the bindings.
    .generate()
    // Unwrap the Result and panic on failure.
    .expect("Unable to generate bindings");

// Write the bindings to the $OUT_DIR/bindings.rs file.
let out_path = PathBuf::from("bindings/");
bindings
    .write_to_file(out_path.join("bindings.rs"))
    .expect("Couldn't write bindings!");
}   