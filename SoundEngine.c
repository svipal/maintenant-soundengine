#include <gdnative_api_struct.gen.h>

#include <string.h>

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <FMOD/fmod.h>
#include <FMOD/fmod_studio.h>


const godot_gdnative_core_api_struct *api = NULL;
const godot_gdnative_ext_nativescript_api_struct *nativescript_api = NULL;



typedef struct se_struct {
    char data[256];
    FMOD_STUDIO_SYSTEM* se_system;
    FMOD_STUDIO_BANK* se_Bank;
    FMOD_STUDIO_EVENTDESCRIPTION* EventMusic;
    FMOD_STUDIO_EVENTINSTANCE* EventInstance;
} se_struct;


void quick_print( const char *str) {
    godot_string gdstr = api->godot_string_chars_to_utf8(str);
    godot_string *nstr = &gdstr;
    api->godot_print(nstr);
    api->godot_string_destroy(nstr);
}

void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *p_options) {
    api = p_options->api_struct;

    // Now find our extensions.
    for (int i = 0; i < api->num_extensions; i++) {
        switch (api->extensions[i]->type) {
            case GDNATIVE_EXT_NATIVESCRIPT: {
                nativescript_api = (godot_gdnative_ext_nativescript_api_struct *)api->extensions[i];
            }; break;
            default: break;
        }
    }
}

void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *p_options) {
    api = NULL;
    nativescript_api = NULL;
}


void *se_constructor(godot_object *p_instance, void *p_method_data) {
    se_struct *sound_engine = api->godot_alloc(sizeof(se_struct));
    strcpy(sound_engine->data, "SoundEngine ready to go");
    fprintf(stderr, "sound_engine starting init");

    FMOD_STUDIO_SYSTEM *system = NULL;
    FMOD_RESULT crea_result = FMOD_Studio_System_Create(&system, FMOD_VERSION);
    switch (crea_result)
        {
            case FMOD_OK:
                fprintf(stderr,"SYSTEM OK\n");
                sound_engine->se_system = system;
                quick_print("FMOD System created");
                break;
            
            default:
                printf("SYSTEM NOT OK\n");
                strcpy(sound_engine->data, "initialization failed");
                return sound_engine;
        }

    FMOD_RESULT init_result = FMOD_Studio_System_Initialize(
        system,
        2,
        FMOD_STUDIO_INIT_LIVEUPDATE,
        FMOD_INIT_NORMAL,
        NULL);
    switch (init_result)
        {
            case FMOD_OK:
                fprintf(stderr,"SYSTEM OK\n");
                sound_engine->se_system = system;
                quick_print("FMOD System initialized");
                break;
            default:
                printf("SYSTEM NOT OK\n");
                strcpy(sound_engine->data, "initialization failed");
                return sound_engine;
        }
    




    //number of midi messages the buffer can hold
    return sound_engine;
}
void se_load_s_bank(godot_object *p_instance, void *p_method_data,
        void *p_se, int p_num_args, godot_variant **p_args) {
    se_struct *sound_engine = (se_struct *)p_se;
    FMOD_STUDIO_BANK* se_bank;
    FMOD_RESULT result = FMOD_Studio_System_LoadBankFile(
        sound_engine->se_system,
        "assets/banks/Destkop/Master.strings.bank",
        FMOD_STUDIO_LOAD_BANK_NORMAL,
        &se_bank
        );
    switch (result)
        {
            case FMOD_OK:
                fprintf(stderr,"Bank load OK\n");
                quick_print("Bank initialized");
                break;
            default:
                printf("Bank NOT OK, error code:\n");
                printf("%d", result);
                strcpy(sound_engine->data, "initialization failed");
                return sound_engine;
        }
}

void se_load_bank(godot_object *p_instance, void *p_method_data,
        void *p_se, int p_num_args, godot_variant **p_args) {
    se_struct *sound_engine = (se_struct *)p_se;
    fprintf(stderr,"hop\n");
    FMOD_STUDIO_BANK* se_bank;
    fprintf(stderr,"hop\n");
    FMOD_RESULT result = FMOD_Studio_System_LoadBankFile(
        sound_engine->se_system,
        "assets/banks/Destkop/Master.bank",
        FMOD_STUDIO_LOAD_BANK_NORMAL,
        &se_bank
        );
    switch (result)
        {
            case FMOD_OK:
                fprintf(stderr,"Bank load OK\n");
                quick_print("Bank initialized");
                break;
            default:
                printf("Bank NOT OK, error code:\n");
                printf("%d", result);
                strcpy(sound_engine->data, "initialization failed");
                return sound_engine;
        }
}


void se_destructor(godot_object *p_instance, void *p_method_data, void *p_se) {
    se_struct *sound_engine = (se_struct *)p_se;

    
    FMOD_Studio_System_Release(sound_engine->se_system);

    quick_print( "system released");
    api->godot_free(p_se);
}



void GDN_EXPORT godot_nativescript_init(void *p_handle) {
    godot_instance_create_func create = { NULL, NULL, NULL };
    create.create_func = &se_constructor;

    godot_instance_destroy_func destroy = { NULL, NULL, NULL };
    destroy.destroy_func = &se_destructor;

    nativescript_api->godot_nativescript_register_class(p_handle, "SoundEngine", "Reference",
            create, destroy);

    godot_instance_method load_bank = { NULL, NULL, NULL };
    load_bank.method = &se_load_bank;

    godot_method_attributes attributes = { GODOT_METHOD_RPC_MODE_DISABLED };

    nativescript_api->godot_nativescript_register_method(p_handle, "SoundEngine", "loadBank",
            attributes, load_bank);

    godot_instance_method load_s_bank = { NULL, NULL, NULL };
    load_bank.method = &se_load_s_bank;

    nativescript_api->godot_nativescript_register_method(p_handle, "SoundEngine", "loadStringBank",
            attributes, load_s_bank);
}

