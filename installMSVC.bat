@echo off
setlocal 
  set RUSTFLAGS="$RUSTFLAGS -A dead_code"
endlocal
cargo build --release && ROBOCOPY "target\release\SoundEngineRust.dll" "..\ExtLib\SoundEngine.dll" /mir